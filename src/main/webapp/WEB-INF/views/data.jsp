<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<body>
<h2>Hello World!</h2>

<c:forEach items="${employees}" var="employee" >
    <tr >
        <td><c:out value="${employee.username}" /></td>
        <td><c:out value="${employee.name}" /></td>
        <td><c:out value="${employee.phone}" /></td>
        <td><c:out value="${employee.email}" /></td>
        <td><c:out value="${employee.department}" /></td>
        <td><c:out value="${employee.rolename}" /></td>
    </tr>
</c:forEach>
</body>
</html>
